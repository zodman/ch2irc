#!/usr/bin/env python
# encoding=utf8
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import ch
import walrus

W = walrus.Database()

class TestBot(ch.RoomManager):
    containers = {}
    def onConnect(self, room):
        print "connect {}".format(room.name)
        self.containers[room.name] = W.List(room.name)
        
    def onEventCalled(self, room, evt, *args, **kw):
#        print room, evt, args, kw
        pass

    def onReconnect(self, room):
#self.redis.rpush(self.key,"Reconnected to "+room.name)
        pass
    def onRaw(self,room, raw):
        print(room, raw)

    def onDisconnect(self, room):
#self.redis.rpush(self.key, "Disconnected from "+room.name)
        msg = "disconnect {}".format(room.name)
        self.containers[room.name].append(msg)
        import sys
        sys.exit(1)

    def onMessage(self, room, user, message):
# Use with PsyfrBot framework? :3
        msg = u"{} : {}".format(user.name,message.body)
        print(msg,)
        self.containers[room.name].append(msg)

if __name__ == "__main__":
    TestBot.easy_start(rooms=["anarchy-subs", "puyeros", "hoshizorasubs"],
          name="desdeirc", password="zxczxc"
        )

