#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function,
                                unicode_literals)

from pyaib.ircbot import IrcBot
from gevent.monkey import patch_subprocess
patch_subprocess()

import sys
import os
argv = sys.argv[1:]
bot = IrcBot(argv[0] if argv else 'bot.conf')
print("Config Dump: %s" % bot.config)


bot.run()


