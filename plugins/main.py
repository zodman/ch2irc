#!/usr/bin/env python
# encoding=utf8
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from pyaib.plugins import  plugin_class, every, keyword
import walrus
from HTMLParser import HTMLParser
from ch import RoomManager
import random

parser = HTMLParser()
msg_send = ""
class ChAnswer(RoomManager):
    def onConnect(self, room): 
        global msg_send
        msg = msg_send
        room.ping()
        print msg
	if ":" in msg:
	    nick, txt = msg.split(":")
	    msg_ = u":".join([nick, txt.decode("utf-8").encode('ascii', 'xmlcharrefreplace')])
	    print msg_send
	    room.message(msg_, html = True)
        

def authorized(func):
    def wrapper(cl, irc_c, msg, trigger, args, kwargs):
        config = irc_c.config
        nick = msg.nick
        if not nick.lower() in config.authorized_nicks:
            msg.reply("wat nigga?")
            return 
        else:
            return func(cl,irc_c,msg, trigger,args, kwargs)
    return wrapper


@plugin_class
class Chatango(object):
    channels = {
            "#anarchy-subs":"anarchy-subs",
            "#puyasubs":"puyeros",
            "#hoshizora-staff":"hoshizorasubs",
           # "#botbot":"puyeros",
           # "#botbot":"anarchy-subs",
            }

    def __init__(self, context, config):
        pass

    @keyword("ch")
    def answer(self, irc_c, msg, trigger, args, kwargs):
        """ message to chatango channel"""
        global msg_send
        rom = self.channels.get(msg.channel.lower(), None)
        if len(args) == 0:
            msg.reply("http://{}.chatango.com".format(rom))
            return 
	plain_text = " ".join(args)
        msg_send = "<b>@%s</b>: %s" % ( msg.nick, plain_text.replace(":", " "))
        if rom:
            ChAnswer.easy_start(rooms=[rom], name="desdeirc", password="zxczxc")

    @every(seconds=1, name="fetch")
    def check_redis(self, *args, **kwargs):
        wal = walrus.Walrus()
        irc_c = args[0].get("client").irc_c
        for chan,room in self.channels.items():
            redis = wal.List(room)
            while redis:
                msg_recived = redis.pop()
                msg  = parser.unescape(msg_recived)
                print (msg_recived,msg)
                irc_c.PRIVMSG(chan, msg)


    @keyword("esputito")
    def esputito(self, irc_c, msg, trigger, args, kwargs):
        """ preguntar si es putito """
        if not "?" in " ".join(args):
            msg.reply("me estas preguntando?... ponle _?_")
            return
        sies = random.choice([True, False, None])
        if sies is None:
            msg.reply(u"Pues quién sabe...")
        elif sies:
            msg.reply(u"Definitivamente reputoooo")
            rom = self.channels.get(msg.channel.lower(), None)
        else:
            msg.reply("Nop")
    

        

    @keyword("die")
    @authorized
    def reiniciar(self, irc_c, msg,*args,**kwargs):
        msg.reply("harakiriii")
        irc_c.client.die()


